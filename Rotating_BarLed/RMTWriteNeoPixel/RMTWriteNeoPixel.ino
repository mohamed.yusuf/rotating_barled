#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "Arduino.h"

#include "esp32-hal.h"


#define NR_OF_LEDS   8*4
#define NR_OF_ALL_BITS 72*NR_OF_LEDS

//
// Note: This example uses Neopixel LED board, 32 LEDs chained one
//      after another, each RGB LED has its 24 bit value 
//      for color configuration (8b for each color)
//
//      Bits encoded as pulses as follows:
//
//      "0":
//         +-------+              +--
//         |       |              |
//         |       |              |
//         |       |              |
//      ---|       |--------------|
//         +       +              +
//         | 0.4us |   0.85 0us   |
//
//      "1":
//         +-------------+       +--
//         |             |       |
//         |             |       |
//         |             |       |
//         |             |       |
//      ---+             +-------+
//         |    0.8us    | 0.4us |

rmt_data_t led_data[NR_OF_ALL_BITS];
rmt_data_t led_data2[NR_OF_ALL_BITS];

rmt_obj_t* rmt_send = NULL;

void setup() 
{
    Serial.begin(115200);
    
    // 18: Pin, true: output or input, RMT_MEM_64: amount of memory
    if ((rmt_send = rmtInit(18, true, RMT_MEM_256)) == NULL)
    {
        Serial.println("init sender failed\n");
    }
    // Set the time unit of rmt, here is 100ns, return the actual set time unit.
    float realTick = rmtSetTick(rmt_send, 100);// 100ns sample-rate
    Serial.printf("real tick set to: %fns\n", realTick);

}

int color[] =  {0x004400,0x440000,0x000044};  // RGB value
int led_index = 0;

void loop() 
{
    // Init data with only one led ON
    int led, col, bit;
    int i=0;
    int n=3;
    for (led=0; led<NR_OF_LEDS; led++) {
      for (col=0; col<n; col++ ) {
            for (bit=0; bit<24; bit++){
                if ( (color[col] & (1<<(23-bit))) && (led == led_index) ) {
                    // First output high level
                    led_data[i].level0 = 1;
                    // High level output time is 8*100ns=0.8 microseconds
                    led_data[i].duration0 = 8;
                    // Output low level
                    led_data[i].level1 = 0;
                    // Low output time is 4*100ns=0.4 microseconds
                    led_data[i].duration1 = 4;

                } else {
                    // bit is 0
                    led_data[i].level0 = 1;
                    led_data[i].duration0 = 4;
                    led_data[i].level1 = 0;
                    led_data[i].duration1 = 8;
                }
                i++;
                
            }
          
        }            
    }

    // travel again
    if ((++led_index)>=NR_OF_LEDS) {
        led_index = 0;       
    } 

    // Send data, Output waveform
    rmtWrite(rmt_send, led_data, NR_OF_ALL_BITS); 
        
 
    delay(100);
    
}
